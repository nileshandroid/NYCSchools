### NYC High School

### Features
1. The Android APP is built using Java framework.
2. The APP works both in online and offline mode 
3. When a user opens NYC high school APP, it list out NYC High schools in ascending order (Card View). A school list contains school name, address, phone number and navigate button.
4. A user can sort NYC Schools by name (asceding, and descending order)
5. A user can contact school by tapping school phone number from the initial screen.
6. A user can navigate to school address by tapping Navigate button from the initial screen.
7. When a user selects a school, a detail view displays 
   - SAT Scores
   - School Overview
   - Other school information (office hours, email, website, total student etc)

![Main Screen](https://gitlab.com/nileshandroid/NYCSchools/-/raw/main/MainScreen.png)
![Detail View Screen](https://gitlab.com/nileshandroid/NYCSchools/-/raw/main/DetailScreen.png)


**REQUIREMENTS**

These requirements are rather high-level and vague. If details are omitted, it is because we will be happy with any of a wide variety of solutions. Don't worry about finding "the" solution. Feel free to be creative with the requirements. Your goal is to impress (but do so with clean code).



1. Display a list of NYC High Schools.
   - Get your data here: https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-pzi2
2. Selecting a school should show additional information about the school
   - Display all the SAT scores - include Math, Reading and Writing.
   - SAT data here: https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4
   - It is up to you to decide what additional information to display

